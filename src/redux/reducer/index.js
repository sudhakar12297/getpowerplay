import { combineReducers } from 'redux';
import beer from "./beer";

const appReducer = combineReducers({
    beer
});

const rootReducer = (state, action) => {
    if (action.type === 'LOGOUT') {
        state = undefined;
    }
    return appReducer(state, action);
};

export default rootReducer;
