let initialState = {
    isLoading: false,
    isError: false,
    randomBeers: [],
    beer:[],
    beers:[],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_RANDOM_BEER_START':
            return { ...state, isLoading: true, isError: false }
        case 'GET_RANDOM_BEER_SUCCESS':
            return { ...state, isLoading: false, isError: false, randomBeers: action.data }
        case 'GET_RANDOM_BEER_FAILURE':
            return { ...state, isLoading: false, isError: true }
        case 'GET_BEERS_START':
            return { ...state, isLoading: true, isError: false }
        case 'GET_BEERS_SUCCESS':
            return { ...state, isLoading: false, isError: false, beers: action.data }
        case 'GET_BEERS_FAILURE':
            return { ...state, isLoading: false, isError: true }
        case 'GET_BEER_BY_ID_START':
            return { ...state, isLoading: true, isError: false }
        case 'GET_BEER_BY_ID_SUCCESS':
            return { ...state, isLoading: false, isError: false, beer: action.data }
        case 'GET_BEER_BY_ID_FAILURE':
            return { ...state, isLoading: false, isError: true }
        default:
            return state
    }
}

export const getIsLoading = state => state.beer.isLoading
export const getIsError = state => state.beer.isError

export const getRandomBeers = state => state.beer.randomBeers
export const getBeers = state => state.beer.beers
export const getBeerById = state => state.beer.beer

