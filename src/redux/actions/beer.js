import { apiCall } from "../../utils/helper";

export const fetchRandomBeer = () => {
    return async dispatch => {
        dispatch({ type: 'GET_RANDOM_BEER_START' })
        console.log('--- inside random beer ---');
        try {
            const data = await apiCall('https://api.punkapi.com/v2/beers/random')
            console.log(data);
            dispatch({ type: 'GET_RANDOM_BEER_SUCCESS', data })
        } catch (e) {
            dispatch({ type: 'GET_RANDOM_BEER_FAILURE' })
        }
    }
}

export const fetchBeers = () => {
    return async dispatch => {
        dispatch({ type: 'GET_BEERS_START' })
        console.log('--- inside beers ---');
        try {
            const data = await apiCall('https://api.punkapi.com/v2/beers')
            console.log(data);
            dispatch({ type: 'GET_BEERS_SUCCESS', data })
        } catch (e) {
            dispatch({ type: 'GET_BEERS_FAILURE' })
        }
    }
}

export const fetchBeerById = (id) => {
    return async dispatch => {
        dispatch({ type: 'GET_BEER_BY_ID_START' })
        console.log('--- inside get beer by id ---');
        try {
            const data = await apiCall(`https://api.punkapi.com/v2/beers/${id}`)
            console.log(data);
            dispatch({ type: 'GET_BEER_BY_ID_SUCCESS', data })
        } catch (e) {
            dispatch({ type: 'GET_BEER_BY_ID_FAILURE' })
        }
    }
}