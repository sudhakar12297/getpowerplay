import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Feed from "../container/feed";
import Dashboard from "../container/dashboard";


export default function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Feed />
                </Route>
                <Route exact path="/feed">
                    <Feed />
                </Route>
                <Route exact path="/dashboard">
                    <Dashboard />
                </Route>
                <Route exact path="/beer/detail">
                    <Dashboard />
                </Route>
            </Switch>
        </Router>
    );
}
