import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { fetchRandomBeer } from '../../redux/actions/beer';
import { getRandomBeers, getIsLoading } from '../../redux/reducer/beer';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));

const Feed = () => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const history = useHistory()

    const randomBeers = useSelector(getRandomBeers)
    const isLoading = useSelector(getIsLoading)

    useEffect(() => {
        dispatch(fetchRandomBeer())
    }, [dispatch])

    // setInterval(() => {
    //     dispatch(fetchRandomBeer())
    // }, 2000)

    const onClickDashboard = () => {
        history.push('/dashboard')
    }

    return (
        <div className={classes.root}>
            <button onClick={onClickDashboard}>Dashboard</button>
            {
                isLoading ? 'Loading...' :
                    randomBeers.map(beer =>
                        <Paper className={classes.paper}>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <ButtonBase className={classes.image}>
                                        <img className={classes.img} alt="complex" src={beer.image_url} />
                                    </ButtonBase>
                                </Grid>
                                <Grid item xs={12} sm container>
                                    <Grid item xs container direction="column" spacing={2}>
                                        <Grid item xs>
                                            <Typography gutterBottom variant="subtitle1">
                                                {beer.name || '---'}
                                            </Typography>
                                            <Typography variant="body2" gutterBottom>
                                                {beer.tagline || '---'}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary">
                                                {beer?.contributed_by || 'Anonymous'}
                                            </Typography>
                                        </Grid>
                                        {/* <Grid item xs container direction="column" spacing={2}>
                                            <Grid item xs>
                                                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                    Remove
                                            </Typography>
                                            </Grid>
                                        </Grid> */}
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="subtitle1">{beer.id}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    )
            }
        </div>
    );
};

export default Feed;
