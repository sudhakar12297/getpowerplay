import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import TextField from '@material-ui/core/TextField';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { fetchBeers } from '../../redux/actions/beer';
import { getBeers, getIsLoading } from '../../redux/reducer/beer';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));


const Dashboard = () => {
    const dispatch = useDispatch()
    const classes = useStyles();

    const [open, setOpen] = useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleClickOpen = (beer) => {
        setSelectedBeer(beer)
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [selectedBeer, setSelectedBeer] = useState('');
    const [localBeers, setLocalBeers] = useState([])
    const [searchValue, setSearchValue] = useState('')

    const beers = useSelector(getBeers)
    const isLoading = useSelector(getIsLoading)

    useEffect(() => {
        dispatch(fetchBeers())
    }, [])

    useEffect(() => {
        setLocalBeers(beers)
    }, [beers])

    const onChangeSearchBar = (e) => {
        setSearchValue(e.target.value);
        const searchResult = beers.filter(beer => beer?.name?.toLowerCase().includes(e.target.value.toLowerCase()))
        setLocalBeers(searchResult)
    }

    return (
        <div className={classes.root}>
            <TextField id="standard-basic" label="Search Beer" value={searchValue} onChange={onChangeSearchBar} />
            {
                isLoading ? 'Loading...' :
                    localBeers.map(beer =>
                        <Paper className={classes.paper} onClick={() => handleClickOpen(beer)}>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <ButtonBase className={classes.image}>
                                        <img className={classes.img} alt="complex" src={beer.image_url} />
                                    </ButtonBase>
                                </Grid>
                                <Grid item xs={12} sm container>
                                    <Grid item xs container direction="column" spacing={2}>
                                        <Grid item xs>
                                            <Typography gutterBottom variant="subtitle1">
                                                {beer.name || '---'}
                                            </Typography>
                                            <Typography variant="body2" gutterBottom>
                                                {beer.tagline || '---'}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary">
                                                {beer?.contributed_by || 'Anonymous'}
                                            </Typography>
                                        </Grid>
                                        {/* <Grid item xs container direction="column" spacing={2}>
                                            <Grid item xs>
                                                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                                    Remove
                                            </Typography>
                                            </Grid>
                                        </Grid> */}
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="subtitle1">{beer.id}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    )
            }

            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{`"${selectedBeer?.name}"`}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        <b>Tagline:</b> {selectedBeer?.tagline}
                    </DialogContentText>
                </DialogContent>
                <DialogContent>
                    <DialogContentText>
                        <b>First Brewed:</b> {selectedBeer?.first_brewed}
                    </DialogContentText>
                </DialogContent>
                <DialogContent>
                    <DialogContentText>
                        <b>Description:</b> {selectedBeer?.description}
                    </DialogContentText>
                </DialogContent>
                <DialogContent>
                    <DialogContentText>
                        <b>Brewers Tips:</b> {selectedBeer?.brewers_tips}
                    </DialogContentText>
                </DialogContent>
                <DialogContent>
                    <DialogContentText>
                        <b>Contributed By:</b> {selectedBeer?.contributed_by}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Cancel
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default Dashboard;