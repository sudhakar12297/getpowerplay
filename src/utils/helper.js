import { ReactReduxContext } from "react-redux"

export const apiCall = async (url) => {
    const response = await fetch(url, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json'
        },
    })
    const data = await response.json()
    if (response.ok) return data
    throw data
}